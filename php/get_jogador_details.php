<?php

$response = array();
// include db connect class
require_once __DIR__ . '/db_connect.php';

$db = new DB_CONNECT();

if (isset($_GET["id"])) {
    $id = $_GET['id'];
    $result = mysql_query("SELECT * FROM jogador WHERE id = $id");
    if (!empty($result)) {
       
        if (mysql_num_rows($result) > 0) {
            $result = mysql_fetch_array($result);
            $pessoas = array();
            $pessoas["id"] = $result["id"];
            $pessoas["nome"] = $result["nome"];
           
            $response["success"] = 1;
            
            $response["objetos"] = array();
            array_push($response["objetos"], $pessoas);
            
            echo json_encode($response);
        } else {
            
            $response["success"] = 0;
            $response["message"] = "Não foram encontrados dados";
            
            echo json_encode($response);
        }
    }
} else {
    $response["success"] = 0;
    $response["message"] = "Por favor, concluir o seu pedido";
    echo json_encode($response);
}
?>