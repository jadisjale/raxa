package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 13/06/2016.
 */
public class EditarRaxaLinhaTempo extends AppCompatActivity {

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_raxa_id = "http://webfate.esy.es/raxa/get_raxa_details_id.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ID = "id";
    private static final String TAG_GOL = "gol";
    private static final String TAG_GOLCONTRA = "golContra";
    private static final String TAG_VITORIA = "vitoria";
    private static final String TAG_DERROTA = "derrota";
    private static final String TAG_DIA = "dia";
    EditText txtData;
    EditText txtGol;
    EditText txtGolContra;
    EditText txtVitoria;
    EditText txtDerrota;
    static String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_raxa_linha_tempo);

        txtData = (EditText) findViewById(R.id.editTextCalendarioE);
        txtGol = (EditText) findViewById(R.id.editTextGolE);
        txtGolContra = (EditText) findViewById(R.id.editTextGolContraE);
        txtVitoria = (EditText) findViewById(R.id.editTextVitoriaE);
        txtDerrota = (EditText) findViewById(R.id.editTextDerrotasE);

        txtData.setEnabled(false);
        txtGol.setEnabled(false);
        txtGolContra.setEnabled(false);
        txtVitoria.setEnabled(false);
        txtDerrota.setEnabled(false);

        Intent i = getIntent();
        id = i.getStringExtra(TAG_ID);

        new GetRaxaById().execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return true;
    }

    class GetRaxaById extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarRaxaLinhaTempo.this);
            pDialog.setMessage("Processando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("id", id));

                        StrictMode.ThreadPolicy policy =
                                new StrictMode.ThreadPolicy.Builder().
                                        permitAll().build();

                        StrictMode.setThreadPolicy(policy);

                        JSONObject json = jsonParser.makeHttpRequest(
                                url_raxa_id, "POST", params);

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {
                            JSONArray productObj = json
                                    .getJSONArray("raxas");
                            JSONObject pendaftaran = productObj
                                    .getJSONObject(0);

                            txtData.setText(pendaftaran.getString(TAG_DIA));
                            txtDerrota.setText(pendaftaran.getString(TAG_DERROTA));
                            txtVitoria.setText(pendaftaran.getString(TAG_VITORIA));
                            txtGol.setText(pendaftaran.getString(TAG_GOL));
                            txtGolContra.setText(pendaftaran.getString(TAG_GOLCONTRA));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

}
