package telas.login.jogador;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jsj.raxa.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by jsj on 04/12/2016.
 */
public class CustomListAdapter extends ArrayAdapter<Usuarios> {

    private static String PATH = "http://webfate.esy.es/raxa/";
    ArrayList<Usuarios> usuarios;
    Context context;
    int resource;

    public CustomListAdapter(Context context, int resource, ArrayList<Usuarios> usuarios) {
        super(context, resource, usuarios);
        this.usuarios = usuarios;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.single_post_jogadores, null, true);

        }
        Usuarios usuario = getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.single_post_jogador_file_resultado_votacao);
        Picasso.with(context)
                .load(PATH + usuario.getFile())
                .placeholder(R.drawable.bola)
                .error(R.drawable.people_default)
                .into(imageView);
        TextView txtName = (TextView) convertView.findViewById(R.id.single_post_jogador_nome_votacao);
        txtName.setText(usuario.getNome());

        TextView txtPrice = (TextView) convertView.findViewById(R.id.single_post_usuario_id);
        txtPrice.setText(usuario.getId());

        return convertView;
    }
}
