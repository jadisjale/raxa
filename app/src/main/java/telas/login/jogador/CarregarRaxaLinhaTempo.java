package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

public class CarregarRaxaLinhaTempo extends AppCompatActivity {

    //list view de raxa do jogador
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> raxaList;
    private static String url_consultas_raxas = "http://webfate.esy.es/raxa/get_raxa_details.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RAXAS = "raxas";
    private static final String TAG_ID = "id";
    private static final String TAG_GOL = "gol";
    private static final String TAG_GOLCONTRA = "golContra";
    private static final String TAG_VITORIA = "vitoria";
    private static final String TAG_DERROTA = "derrota";
    private static final String TAG_DIA = "dia";
    private String idJogador;

    JSONArray empresaJson = null;
    ListView lv;
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.linha_do_tempo);
        raxaList = new ArrayList<>();
        new CarregarListaRaxa().execute();
        lv = (ListView) findViewById(R.id.listViewRaxas_linha_tempo);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idRaxa = ((TextView) view.findViewById(R.id.single_post_gol_id_linha_tempo)).getText()
                        .toString();
                
                Intent in = new Intent(getApplicationContext(), EditarRaxaLinhaTempo.class);
                in.putExtra(TAG_ID, idRaxa);
                startActivity(in);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_linha_tempo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;
        switch (id) {

            case android.R.id.home:
                finish();
            break;

            case R.id.resultado_linha_tempo:
                i = new Intent(getApplicationContext(), SeuResultadoLinhaTempo.class);
                startActivity(i);
            break;

            case R.id.comparacao:
                i = new Intent(getApplicationContext(), Comparacao.class);
                startActivity(i);
//                Toast.makeText(getApplicationContext(), "Comparação", Toast.LENGTH_LONG).show();
            break;
        }

        return true;

    }

    class CarregarListaRaxa extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = CarregarRaxaLinhaTempo.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("jogador", JogadorLogado.idJodagorLinhaTempo));
            JSONObject json = jParser.makeHttpRequest(url_consultas_raxas, "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_RAXAS);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String data = c.getString(TAG_DIA);
                        String vitoria = c.getString(TAG_VITORIA);
                        String gol = c.getString(TAG_GOL);

                        HashMap<String, String> map = new HashMap<>();

                        map.put(TAG_ID, id);
                        map.put(TAG_DIA, data);
                        map.put(TAG_VITORIA, vitoria);
                        map.put(TAG_GOL, gol);

                        raxaList.add(map);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * json atualizado para ArrayList
                     * */
                    adapter = new SimpleAdapter(
                            CarregarRaxaLinhaTempo.this, raxaList,
                            R.layout.single_post_raxas_linha_tempo, new String[] {TAG_ID, TAG_DIA,
                            TAG_VITORIA, TAG_GOL},
                            new int[] {
                                    R.id.single_post_gol_id_linha_tempo,
                                    R.id.single_post_data_raxa_linha_tempo,
                                    R.id.single_post_vitoria_raxa_linha_tempo,
                                    R.id.single_post_gol_raxa_linha_tempo});
                    lv.setAdapter(adapter);

                    EditText filter =
                            (EditText) findViewById(R.id.edtTextFiltroRaxa_linha_tempo);

                    assert filter != null;

                    filter.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(s.length() > 1) {
                                ((SimpleAdapter) CarregarRaxaLinhaTempo.this.adapter).getFilter().filter(s.toString());
                            }

                            if(s.length() == 0) {
                                ((SimpleAdapter) CarregarRaxaLinhaTempo.this.adapter).getFilter().filter(s.toString());
                            }

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                }
            });

        }

    }

}
