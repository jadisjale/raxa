package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 28/06/2016.
 */
public class RankNegativoVitoria extends AppCompatActivity {

    Button voltar;

    TextView cat;
    TextView nome1;
    TextView gol1;

    TextView nome2;
    TextView gol2;

    TextView nome3;
    TextView gol3;

    List<String> nomes;
    List<String> ids;

    String categoria;

    JSONArray empresaJson = null;

    JSONParser jParser = new JSONParser();
    private static String url_consultas_rank_negativo_jogo = "http://webfate.esy.es/raxa/get_rank_negativo_jogo.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_IDS = "ids";
    private static final String TAG_NOME = "nome";
    private static final String TAG_RAXAS = "raxas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rank_gol);


        nomes = new ArrayList<>();
        ids = new ArrayList<>();

        Intent i = getIntent();
        categoria = i.getStringExtra("vitoria");
        cat = (TextView) findViewById(R.id.txtCategoria);
        cat.setText(categoria);

        nome1 = (TextView) findViewById(R.id.txtNomeRankGol1);
        nome2 = (TextView) findViewById(R.id.txtNomeRankGol2);
        nome3 = (TextView) findViewById(R.id.txtNomeRankGol3);

        gol1 = (TextView) findViewById(R.id.txtGolRank1);
        gol2 = (TextView) findViewById(R.id.txtGolRank2);
        gol3 = (TextView) findViewById(R.id.txtGolRank3);

        new CarregarResultadoRankGol().execute();

        voltar = (Button) findViewById(R.id.buttonVoltarRankGol);

        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return true;
    }


    class CarregarResultadoRankGol extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = RankNegativoVitoria.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Calculando resultado...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            JSONObject json = jParser.makeHttpRequest(url_consultas_rank_negativo_jogo, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_RAXAS);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);
                        String nome = c.getString(TAG_NOME);
                        String vitoria = c.getString(TAG_IDS);
                        nomes.add(nome);
                        ids.add(vitoria);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            this.pDialog.dismiss();
            nome1.setText(nomes.get(0));
            nome2.setText(nomes.get(1));
            nome3.setText(nomes.get(2));

            gol1.setText(ids.get(0));
            gol2.setText(ids.get(1));
            gol3.setText(ids.get(2));
            }
        }
}
