package telas.login.jogador;

/**
 * Created by jsj on 04/12/2016.
 */
public class Usuarios {

    private String nome;
    private String file;
    private String id;

    public Usuarios(String nome, String file, String id) {
        this.nome = nome;
        this.file = file;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
