package telas.login.jogador;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 25/11/2016.
 */
public class Votacao extends AppCompatActivity {

    ArrayList<Usuarios> listaUsuarios = new ArrayList<>();
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> raxaList;
    private static String url_consultas_jogador = "http://webfate.esy.es/raxa/get_all_jogador_menos_logado.php";
    private static String url_salvar_votacao = "http://webfate.esy.es/raxa/save_votacao.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_OBJETOS = "objetos";
    private static final String TAG_ID = "id";
    private static final String TAG_NOME = "nome";
    private static final String TAG_FILE = "file";
    private String enquete_id;
    private String pergunta;
    private AlertDialog alerta;
    private String jogador_id_votado;
    JSONArray empresaJson = null;
    ListView lv;
//    private Button voltar;
    private TextView txtPergunta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_jogador);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        enquete_id = bundle.getString("enquete_id");
        pergunta = bundle.getString("pergunta");
        raxaList = new ArrayList<>();
        new CarregarListajogador().execute();
        lv = (ListView) findViewById(R.id.listViewJogador);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                jogador_id_votado = ((TextView) view.findViewById(R.id.single_post_usuario_id)).getText()
                        .toString();
                AlertDialog.Builder builder = new AlertDialog.Builder(Votacao.this);
                builder.setTitle("Enquete");
                builder.setMessage("Deseja votar nesse jogador");
                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        new SalvarVotacao().execute();
                    }
                });

                builder.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Toast.makeText(getApplicationContext(), "Escolher outro", Toast.LENGTH_SHORT).show();
                    }
                });
                alerta = builder.create();
                alerta.show();

            }
        });

//        voltar = (Button) findViewById(R.id.voltar_menu);
//        voltar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                startActivity(intent);
//            }
//        });

        txtPergunta = (TextView) findViewById(R.id.txtEnquete);
        assert txtPergunta != null;
        txtPergunta.setText(pergunta);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;
        switch (id) {
            case android.R.id.home:
                i = new Intent(getApplicationContext(), Enquete.class);
                startActivity(i);
                finish();
                return (true);
        }

        return true;
    }

    class CarregarListajogador extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = Votacao.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {
            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("id", String.valueOf(JogadorLogado.idJogador)));
                    JSONObject json = jParser.makeHttpRequest(url_consultas_jogador, "POST", params);

                    try {
                        int success = json.getInt(TAG_SUCCESS);

                        if (success == 1) {
                            empresaJson = json.getJSONArray(TAG_OBJETOS);

                            for (int i = 0; i < empresaJson.length(); i++) {
                                JSONObject c = empresaJson.getJSONObject(i);

                                String id = c.getString(TAG_ID);
                                String nome = c.getString(TAG_NOME);
                                String file = c.getString(TAG_FILE);

                                listaUsuarios.add(new Usuarios(
                                        nome,
                                        file,
                                        id
                                ));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CustomListAdapter adapter = new CustomListAdapter(
                            getApplicationContext(), R.layout.single_post_jogadores, listaUsuarios
                    );
                    lv.setAdapter(adapter);
                }
            });

        }
    }

    class SalvarVotacao extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = Votacao.this;
        JSONParser jsonParser = new JSONParser();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Votacao.this);
            pDialog.setMessage("Cadastrando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("jogador_id", JogadorLogado.idJogador.toString()));
            params.add(new BasicNameValuePair("jogador_id_votado", jogador_id_votado));
            params.add(new BasicNameValuePair("enquete_id", enquete_id));


            JSONObject json = jsonParser.
                    makeHttpRequest(url_salvar_votacao,
                            "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    finish();
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Vota��o conclu�da.", Toast.LENGTH_SHORT).show();
        }

    }
}
