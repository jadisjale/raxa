package telas.login.jogador;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import json.JSONParser;

/**
 * Created by jsj on 11/06/2016.
 */
public class CadastrarRaxa extends AppCompatActivity {

    static final int DATE_DIALOG_ID = 0;
    private ImageButton btnCalendario;
    private Button btnSalvar;
    private EditText editTextCalendario;
    private EditText Egols;
    private EditText Egolscontra;
    private EditText Evitorias;
    private EditText Ederrotas;

    private String data = "";
    private String gols;
    private String golscontra;
    private String vitorias;
    private String derrotas;

    JSONParser jsonParser = new JSONParser();

    private ProgressDialog pDialog;

    private static String url_salvar_raxa = "http://webfate.esy.es/raxa/create_raxa.php";
    private static final String TAG_SUCCESS = "success";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastrar_raxa);

        Egols = (EditText) findViewById(R.id.teste_linha_tempo);
        Egolscontra = (EditText) findViewById(R.id.tesda);
        Evitorias = (EditText) findViewById(R.id.editTextVitoriaE);
        Ederrotas = (EditText) findViewById(R.id.editTextDerrotas);

        editTextCalendario = (EditText) findViewById(R.id.editTextCalendarioE_linha_tempo);
        assert editTextCalendario != null;
        editTextCalendario.setEnabled(false);

        btnCalendario = (ImageButton) findViewById(R.id.btnCalendarioE);
        btnCalendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        btnSalvar = (Button) findViewById(R.id.buttonSalvarRaxa);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gols = Egols.getText().toString();
                golscontra = Egolscontra.getText().toString();
                vitorias = Evitorias.getText().toString();
                derrotas = Ederrotas.getText().toString();

                if(!data.equals("")){
                    if(!gols.equals("")){
                        if(!golscontra.equals("")){
                            if(!vitorias.equals("")){
                                if(!derrotas.equals("")){
                                    new CadastrarNewRaxa().execute();
                                } else {
                                    menssagem("Campo derrota abrigatório");
                                }
                            } else {
                                menssagem("Campo vitória abrigatório");
                            }
                        } else {
                            menssagem("Campo golContra abrigatório");
                        }
                    } else {
                        menssagem("Campo gol abrigatório");
                    }
                } else {
                    menssagem("Campo data abrigatório");
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return (true);
        }

        return true;
    }

    public void menssagem (String menssagem){
        Toast.makeText(getApplicationContext(), menssagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calendario = Calendar.getInstance();

        int ano = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, ano, mes,
                        dia);
        }
        return null;
    }


    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String d = String.valueOf(dayOfMonth);
            String m =  String.valueOf(monthOfYear+1);
            String a =  String.valueOf(year);

            String dia = null;
            String mes = null;

            if (d.length() < 2) {
                dia = "0"+d;
            } else {
                dia = d;
            }

            if(m.length() < 2) {
                mes = "0"+m;
            } else {
                mes = m;
            }


            editTextCalendario.setText(dia + "/" + mes + "/" + a);
            data = dia + "/" + mes + "/" + a;
        }
    };

    class CadastrarNewRaxa extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CadastrarRaxa.this);
            pDialog.setMessage("Cadastrando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("dia", data));
            params.add(new BasicNameValuePair("gol", gols));
            params.add(new BasicNameValuePair("golContra", golscontra));
            params.add(new BasicNameValuePair("vitoria", vitorias));
            params.add(new BasicNameValuePair("derrota", derrotas));
            params.add(new BasicNameValuePair("jogador", String.valueOf(JogadorLogado.idJogador)));

            JSONObject json = jsonParser.
                    makeHttpRequest(url_salvar_raxa,
                            "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }
}
