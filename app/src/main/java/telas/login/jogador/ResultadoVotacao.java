package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 25/11/2016.
 */
public class ResultadoVotacao extends AppCompatActivity {

    ArrayList<JogadoresVotacao> listaUsuarios = new ArrayList<>();
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> raxaList;
    private static String url_lista_votacao = "http://webfate.esy.es/raxa/resultado_votacao.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_OBJETOS = "votacao";
    private static final String TAG_ID = "jogador_id_votado";
    private static final String TAG_NOME = "nome";
    private static final String TAG_VOTOS = "votos";
    private static final String TAG_TOTAL = "total";
    private static final String TAG_PORCENTAGEM = "porcentagem";
    private static final String TAG_FILE = "file";
    private String pergunta;
    private String enquete_id;
    JSONArray empresaJson = null;
    ListView lv;
    private TextView txtPergunta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_resultado_votacao);
        raxaList = new ArrayList<>();
        new CarregarListaResultadoVotacao().execute();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        enquete_id = bundle.getString("enquete_id");
        pergunta = bundle.getString("pergunta");
        lv = (ListView) findViewById(R.id.listResultadoVotacao);
        txtPergunta = (TextView) findViewById(R.id.txtPergunta);
        assert txtPergunta != null;
        txtPergunta.setText(pergunta);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return true;
    }

    class CarregarListaResultadoVotacao extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = ResultadoVotacao.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {
            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("enquete_id", enquete_id));
                    JSONObject json = jParser.makeHttpRequest(url_lista_votacao, "POST", params);

                    try {
                        int success = json.getInt(TAG_SUCCESS);

                        if (success == 1) {
                            empresaJson = json.getJSONArray(TAG_OBJETOS);

                            for (int i = 0; i < empresaJson.length(); i++) {
                                JSONObject c = empresaJson.getJSONObject(i);

                                String id = c.getString(TAG_ID);
                                String file = c.getString(TAG_FILE);
                                String nome = c.getString(TAG_NOME);
                                String votos = c.getString(TAG_VOTOS);
                                String total = c.getString(TAG_TOTAL);
                                String porcentagem = c.getString(TAG_PORCENTAGEM);

                                listaUsuarios.add(new JogadoresVotacao(
                                        nome,
                                        file,
                                        id,
                                        votos,
                                        total,
                                        porcentagem
                                ));

                                Log.i("nome_lista", nome);
                                Log.i("porcentagem", porcentagem);
                                Log.i("enquete", enquete_id);

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CustomListAdapterVotacao adapter = new CustomListAdapterVotacao(
                            getApplicationContext(), R.layout.single_post_jogadores_votacao, listaUsuarios
                    );
                    lv.setAdapter(adapter);
                }
            });

        }
    }
}
