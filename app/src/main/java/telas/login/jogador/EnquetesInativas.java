package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

public class EnquetesInativas extends AppCompatActivity {

    //list view de raxa do jogador
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> listEnquete;
    private static String url_consultas_enquete_inativa = "http://webfate.esy.es/raxa/get_all_enquete_inativa.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_DATA = "data";
    private static final String TAG_ID = "id";
    private static final String TAG_INICIO = "data_inicio";
    private static final String TAG_FIM = "data_fim";
    private static final String TAG_PERGUNTA = "pergunta";
    private int success;

    JSONArray empresaJson = null;
    ListView lv;
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_enquete_inativa);
        listEnquete = new ArrayList<>();
        new CarregarEnquetesInativas().execute();
        lv = (ListView) findViewById(R.id.listEnqueteInativa);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idEnquete = ((TextView) view.findViewById(R.id.single_post_enquete_inativa)).getText()
                        .toString();
                String pergunta = ((TextView) view.findViewById(R.id.single_post_enquete)).getText()
                        .toString();
                Intent intent = new Intent(getApplicationContext(), ResultadoVotacao.class);
                Bundle bundle = new Bundle();
                bundle.putString("enquete_id", idEnquete);
                bundle.putString("pergunta", pergunta);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;
        switch (id) {
            case android.R.id.home:
                i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                return (true);
        }

        return true;
    }

    class CarregarEnquetesInativas extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = EnquetesInativas.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            JSONObject json = jParser.makeHttpRequest(url_consultas_enquete_inativa, "GET", params);

            try {
                success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_DATA);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String data_inicio = c.getString(TAG_INICIO);
                        String data_fim = c.getString(TAG_FIM);
                        String pergunta = c.getString(TAG_PERGUNTA);

                        HashMap<String, String> map = new HashMap<>();

                        map.put(TAG_ID, id);
                        map.put(TAG_INICIO, data_inicio);
                        map.put(TAG_FIM, data_fim);
                        map.put(TAG_PERGUNTA, pergunta);

                        listEnquete.add(map);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (success == 0){
                Toast.makeText(getApplicationContext(), "Nenhuma enquete finalizada", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        /**
                         * json atualizado para ArrayList
                         * */
                        adapter = new SimpleAdapter(
                                EnquetesInativas.this, listEnquete,
                                R.layout.single_post_enquete_inativa, new String[] {TAG_ID, TAG_INICIO,
                                TAG_FIM, TAG_PERGUNTA},
                                new int[] {
                                        R.id.single_post_enquete_inativa,
                                        R.id.single_post_dataInicio,
                                        R.id.single_post_dataFim,
                                        R.id.single_post_enquete});
                        lv.setAdapter(adapter);
                    }
                });
            }
        }

    }

}
