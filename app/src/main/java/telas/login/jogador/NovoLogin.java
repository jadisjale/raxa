package telas.login.jogador;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jsj.raxa.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by jsj on 05/11/2016.
 */
public class NovoLogin extends AppCompatActivity {

    private static String PATH = "http://webfate.esy.es/raxa/";
    private Button login;
    private EditText senha;
    private TextView nome;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.novo_login);

        login = (Button) findViewById(R.id.loginFile);
        nome = (TextView) findViewById(R.id.nomeJogador);
        assert nome != null;
        nome.setText(JogadorLogado.nomeJogador);
        
        image = (ImageView) findViewById(R.id.view8);
        Picasso.with(getApplicationContext())
                .load(PATH + JogadorLogado.file)
                .resize(600, 200) // resizes the image to these dimensions (in pixel)
                .centerInside()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.bola)
                .error(R.drawable.people_default)
                .into(image);

        senha = (EditText) findViewById(R.id.senhaNovoLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String senhaInput = senha.getText().toString();
                if(senhaInput.equals(JogadorLogado.senhaJogador)) {
                    Intent intent = new Intent(getApplicationContext(), Splash.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Senha errada, tente novamente", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
