package telas.login.jogador;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jsj.raxa.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by jsj on 04/12/2016.
 */
public class CustomListAdapterVotacao extends ArrayAdapter<JogadoresVotacao> {

    private static String PATH = "http://webfate.esy.es/raxa/";
    ArrayList<JogadoresVotacao> usuarios;
    Context context;
    int resource;

    public CustomListAdapterVotacao(Context context, int resource, ArrayList<JogadoresVotacao> usuarios) {
        super(context, resource, usuarios);
        this.usuarios = usuarios;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.single_post_jogadores_votacao, null, true);

        }
        JogadoresVotacao usuario = getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.single_post_jogador_file_resultado_votacao);
        Picasso.with(context)
                .load(PATH + usuario.getFile())
                .placeholder(R.drawable.bola)
                .error(R.drawable.people_default)
                .into(imageView);
        TextView txtName = (TextView) convertView.findViewById(R.id.single_post_jogador_nome_votacao);
        txtName.setText(usuario.getNome());

        TextView txtVoto = (TextView) convertView.findViewById(R.id.votos_votacao);
        txtVoto.setText(usuario.getVoto());

        TextView total = (TextView) convertView.findViewById(R.id.total_votacao);
        total.setText(usuario.getTotal());

        TextView porcentagem = (TextView) convertView.findViewById(R.id.porcentagem_votacao);
        Double por;
        por = Double.parseDouble(usuario.getPorcentagem());
        porcentagem.setText(format(por)+"%");

        return convertView;

    }

    public String format (Double number) {
        String result = String.format("%s", number);
        Log.i("numero", result);
        if (result.length() > 4){
            return result.substring(0, 5);
        } else {
            return result;
        }
    }
}
