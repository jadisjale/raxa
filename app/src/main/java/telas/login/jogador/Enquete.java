package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 19/11/2016.
 */
public class Enquete extends AppCompatActivity {

    private Intent i;
    private TextView pergunta;
    private TextView dataInicio;
    private TextView dataFim;

    private String sPergunta = "N�o existe pergunta ativa";
    private String sDataInicio = "";
    private String sDataFim = "";
    private String sId = "";
    public Integer resposta;

    JSONArray enqueteJson = null;
    JSONParser jParser = new JSONParser();
    private static final String TAG_SUCCESS = "success";
    private static final String PERGUNTA = "pergunta";
    private static final String INICIO = "data_inicio";
    private static final String FIM = "data_fim";
    private static final String ENQUETE_ID = "id";
    private static final String ENQUETE = "objetos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enquete);

        Button enquete = (Button) findViewById(R.id.enquete);

        pergunta = (TextView) findViewById(R.id.pergunta);
        dataInicio = (TextView) findViewById(R.id.data_inicio);
        dataFim = (TextView) findViewById(R.id.data_fim);

        new EnqueteAtiva().execute();

        assert enquete != null;
        enquete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new VerificarJogadorEnquete().execute();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;
        switch (id) {
            case android.R.id.home:
                i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                return (true);
        }

        return true;
    }

    class EnqueteAtiva extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = Enquete.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Verificando enquete...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            String enquete_ativa = "http://webfate.esy.es/raxa/get_enquete_ativa.php";
            JSONObject json = jParser.makeHttpRequest(enquete_ativa, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    resposta = 1;
                    enqueteJson = json.getJSONArray(ENQUETE);

                    for (int i = 0; i < enqueteJson.length(); i++) {
                        JSONObject c = enqueteJson.getJSONObject(i);
                        sPergunta = c.getString(PERGUNTA);
                        sDataInicio = c.getString(INICIO);
                        sDataFim = c.getString(FIM);
                        sId = c.getString(ENQUETE_ID);
                    }
                } else {
                    resposta = 2;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            this.pDialog.dismiss();
            pergunta.setText(sPergunta);
            dataInicio.setText(sDataInicio);
            dataFim.setText(sDataFim);

            if (resposta == 2) {
                i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                Toast.makeText(getApplicationContext(), "N�o exite enquete ativa", Toast.LENGTH_LONG).show();
            }
        }
    }

    class VerificarJogadorEnquete extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = Enquete.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Verificando...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            //parametros via post
            params.add(new BasicNameValuePair("jogador", JogadorLogado.idJogador.toString()));
            params.add(new BasicNameValuePair("enquete", sId));
            //passando os parametros pra url
            String verifica_votacao = "http://webfate.esy.es/raxa/verifica_votacao.php";
            JSONObject json = jParser.makeHttpRequest(verifica_votacao, "POST", params);

            try {
                Integer success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    resposta = 1;
                } else {
                    resposta = 2;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            this.pDialog.dismiss();
            if(resposta == 1){
                Toast.makeText(getApplicationContext(), "Por favor, aguarde uma nova enquete", Toast.LENGTH_LONG).show();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString("enquete_id", sId);
                bundle.putString("pergunta", sPergunta);
                i = new Intent(getApplicationContext(), Votacao.class);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        }
    }
}

