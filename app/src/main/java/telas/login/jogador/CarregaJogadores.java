package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 25/11/2016.
 */
public class CarregaJogadores extends AppCompatActivity {

    ArrayList<Usuarios> listaUsuarios = new ArrayList<>();
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> raxaList;
    private static String url_consultas_jogador = "http://webfate.esy.es/raxa/get_all_jogador_menos_logado.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_OBJETOS = "objetos";
    private static final String TAG_ID = "id";
    private static final String TAG_NOME = "nome";
    private static final String TAG_FILE = "file";
    private String idJogador;
    JSONArray empresaJson = null;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_jogador_all);
        raxaList = new ArrayList<>();
        new CarregarListajogador().execute();
        lv = (ListView) findViewById(R.id.listViewJogadorAll);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                idJogador = ((TextView) view.findViewById(R.id.single_post_usuario_id_all)).getText()
                        .toString();

                JogadorLogado.nomeJodagorLinhaTempo = ((TextView) view.findViewById(R.id.single_post_jogador_nome_votacao_all)).getText()
                        .toString();
                JogadorLogado.idJodagorLinhaTempo = idJogador;
                Intent intent = new Intent(getApplicationContext(), CarregarRaxaLinhaTempo.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return (true);
        }

        return true;
    }

    class CarregarListajogador extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = CarregaJogadores.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {
            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("id", String.valueOf(JogadorLogado.idJogador)));
                    JSONObject json = jParser.makeHttpRequest(url_consultas_jogador, "POST", params);

                    try {
                        int success = json.getInt(TAG_SUCCESS);

                        if (success == 1) {
                            empresaJson = json.getJSONArray(TAG_OBJETOS);

                            for (int i = 0; i < empresaJson.length(); i++) {
                                JSONObject c = empresaJson.getJSONObject(i);

                                String id = c.getString(TAG_ID);
                                String nome = c.getString(TAG_NOME);
                                String file = c.getString(TAG_FILE);

                                listaUsuarios.add(new Usuarios(
                                        nome,
                                        file,
                                        id
                                ));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CustomListAdapterAll adapter = new CustomListAdapterAll(
                            getApplicationContext(), R.layout.single_post_jogadores_all, listaUsuarios
                    );
                    lv.setAdapter(adapter);
                }
            });

        }
    }

}
