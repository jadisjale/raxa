package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jsj.raxa.Login;
import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 07/06/2016.
 */
public class SalvarJogador extends AppCompatActivity {

    private Button btnSalvar;
    private EditText nomeE;
    private EditText senhaE;

    private String nome;
    private String senha;

    public ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static String url_salvar_jogador = "http://webfate.esy.es/raxa/create_jogador.php";
    private static final String TAG_SUCCESS = "success";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.save_jogador);

        nomeE = (EditText) findViewById(R.id.jogadorNameS);
        senhaE = (EditText) findViewById(R.id.jogadorPasswordS);
        btnSalvar = (Button) findViewById(R.id.btnSaveJogador);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nome = nomeE.getText().toString();
                senha = senhaE.getText().toString();
                if(nome.length() > 2){
                    if(senha.length() > 2 ){
                        new CadastrarNewJogador().execute();
                    } else {
                        menssagem("Campo Senha abrigatório ou insuficiente");
                    }

                } else {
                    menssagem("Campo Nome abrigatório ou insuficiente");
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return true;
    }


    public void menssagem (String menssagem){
        Toast.makeText(getApplicationContext(), menssagem, Toast.LENGTH_SHORT).show();
    }

    class CadastrarNewJogador extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SalvarJogador.this);
            pDialog.setMessage("Cadastrando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("nome", nome));
            params.add(new BasicNameValuePair("senha", senha));


            JSONObject json = jsonParser.
                    makeHttpRequest(url_salvar_jogador,
                            "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    finish();
                    Intent i = new Intent(getApplicationContext(), Login.class);
                    startActivity(i);
                }
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }
}
