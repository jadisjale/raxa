package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 24/06/2016.
 */
public class SeuResultado extends AppCompatActivity {

    private Button voltar;
    public TextView gol;
    public TextView vitoria;
    public TextView derrota;
    public TextView golContra;
    public TextView golMedia;
    public TextView vitoriaMedia;
    public TextView derrotaMedia;
    public TextView golContraMedia;
    public TextView numeroRaxa;
    String golContraData = "";
    String golData = "";
    String derrotaData = "";
    String vitoriaData = "";

    Float golContraDataMedia;
    Float golDataMedia;
    Float derrotaDataMedia;
    Float vitoriaDataMedia;

    String id = "";

    private static String url_consultas_jogador = "http://webfate.esy.es/raxa/get_result_jogador.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RAXAS = "raxas";
    private static final String TAG_GOL = "gol";
    private static final String TAG_ID = "id";
    private static final String TAG_GOLCONTRA = "golContra";
    private static final String TAG_VITORIA = "vitoria";
    private static final String TAG_DERROTA = "derrota";

    JSONArray empresaJson = null;

    JSONParser jParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_jogador);

        voltar = (Button) findViewById(R.id.btnJogador);
        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SeuResultado.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        gol = (TextView) findViewById(R.id.txtGolResultadods);
        vitoria = (TextView) findViewById(R.id.txtVitoriaResultado);
        golContra = (TextView) findViewById(R.id.txtDerrotaResultado);
        derrota = (TextView) findViewById(R.id.golContraResultado);

        golMedia = (TextView) findViewById(R.id.golMedia);
        vitoriaMedia = (TextView) findViewById(R.id.vitoriaMedia);
        golContraMedia = (TextView) findViewById(R.id.golContraMedia);
        derrotaMedia = (TextView) findViewById(R.id.derrotaMedia);

        numeroRaxa = (TextView) findViewById(R.id.numeroRaxa);

        new CarregarResultado().execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;
        switch (id) {
            case android.R.id.home:
                i = new Intent(getApplication(), MainActivity.class);
                startActivity(i);
                finish();
                return true;
        }

        return true;
    }


    class CarregarResultado extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = SeuResultado.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Calculando resultado...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("jogador", String.valueOf(JogadorLogado.idJogador)));
            JSONObject json = jParser.makeHttpRequest(url_consultas_jogador, "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_RAXAS);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);

                        id = c.getString(TAG_ID);
                        vitoriaData = c.getString(TAG_VITORIA);
                        golData = c.getString(TAG_GOL);
                        derrotaData = c.getString(TAG_DERROTA);
                        golContraData = c.getString(TAG_GOLCONTRA);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            this.pDialog.dismiss();

            if (!golData.equals("null")) {
                gol.setText(golData);
            } else {
                gol.setText("0");
            }

            if (!derrotaData.equals("null")) {
                derrota.setText(derrotaData);
            } else {
                derrota.setText("0");
            }

            if (!vitoriaData.equals("null")) {
                vitoria.setText(vitoriaData);
            } else {
                vitoria.setText("0");
            }

            if (!golContraData.equals("null")) {
                golContra.setText(golContraData);
            } else {
                golContra.setText("0");
            }

            int media = Integer.parseInt(id);

            if (!golData.equals("null")){
                golMedia.setText(format(Double.parseDouble(golData) / media));
            }

            if(!derrotaData.equals("null")){
                derrotaMedia.setText(format(Double.parseDouble(derrotaData) / media));
            }

            if(!vitoriaData.equals("null")){
                vitoriaMedia.setText(format(Double.parseDouble(vitoriaData) / media));
            }

            if(!golContraData.equals("null")){
                golContraMedia.setText(format(Double.parseDouble(golContraData) / media));
            }
            numeroRaxa.setText(id);
        }

    }

    public String format (Double number) {
       String result = String.format("%s", number);
        if (result.length() > 4){
            return result.substring(0, 4);
        } else {
            return result;
        }
    }
}
