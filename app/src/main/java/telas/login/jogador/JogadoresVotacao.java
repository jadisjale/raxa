package telas.login.jogador;

/**
 * Created by jsj on 11/12/2016.
 */
public class JogadoresVotacao extends Usuarios {

    private String voto;
    private String total;
    private String porcentagem;

    public JogadoresVotacao(String nome, String file, String id, String voto, String total, String porcentagem) {
        super(nome, file, id);
        this.total = total;
        this.voto = voto;
        this.porcentagem = porcentagem;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPorcentagem() {
        return porcentagem;
    }

    public void setPorcentagem(String porcentagem) {
        this.porcentagem = porcentagem;
    }
}
