package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.jsj.raxa.Login;
import com.example.jsj.raxa.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //list view de raxa do jogador
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> raxaList;
    private static String url_consultas_raxas = "http://webfate.esy.es/raxa/get_raxa_details.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RAXAS = "raxas";
    private static final String TAG_ID = "id";
    private static final String TAG_GOL = "gol";
    private static final String TAG_GOLCONTRA = "golContra";
    private static final String TAG_VITORIA = "vitoria";
    private static final String TAG_DERROTA = "derrota";
    private static final String TAG_DIA = "dia";
    ImageView foto;
    TextView nome_principal;

    JSONArray empresaJson = null;
    ListView lv;
    ListAdapter adapter;
    private static String PATH = "http://webfate.esy.es/raxa/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        raxaList = new ArrayList<>();
        new CarregarListaRaxa().execute();
        lv = (ListView) findViewById(R.id.listViewRaxas);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idRaxa = ((TextView) view.findViewById(R.id.single_post_gol_id)).getText()
                        .toString();

                Intent in = new Intent(getApplicationContext(),
                        EditarRaxa.class);
                in.putExtra(TAG_ID, idRaxa);
                startActivity(in);
                finish();
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CadastrarRaxa.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View hView =  navigationView.inflateHeaderView(R.layout.nav_header_main);
        ImageView imgvw = (ImageView) hView.findViewById(R.id.foto_jogador_principal);
        TextView tv = (TextView) hView.findViewById(R.id.nome_jogador_principal);
        tv.setText(JogadorLogado.nomeJogador);
        Picasso.with(getApplicationContext())
                .load(PATH + JogadorLogado.file)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.bola)
                .error(R.drawable.people_default)
                .into(imgvw);

        Log.i("load", PATH + JogadorLogado.file);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.resultados) {
            intent = new Intent(getApplicationContext(), SeuResultado.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.sair) {
            Intent i = new Intent(getApplicationContext(), Login.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            return true;
        }

        if (id == R.id.atualizar) {
            intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        if (id == R.id.perfil) {
            intent = new Intent(getApplicationContext(), AlterarJogador.class);
            startActivity(intent);
            finish();
            return true;
        }

        if (id == R.id.enquete) {
            intent = new Intent(getApplicationContext(), Enquete.class);
            startActivity(intent);
            finish();
            return true;
        }

        if (id == R.id.resultadoVotacao) {
            intent = new Intent(getApplicationContext(), EnquetesInativas.class);
            startActivity(intent);
            finish();
            return true;
        }

        if (id == R.id.jogadores) {
            intent = new Intent(getApplicationContext(), CarregaJogadores.class);
            startActivity(intent);
//            Toast.makeText(getApplicationContext(), "Jogadores", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent intent;

        if (id == R.id.nav_vitoria) {
            intent = new Intent(getApplicationContext(), RankVitoria.class);
            intent.putExtra("vitoria", "Vitoriosos");
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_gol) {
            intent = new Intent(getApplicationContext(), RankGol.class);
            intent.putExtra("gol", "Goleadores");
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_jogos) {
            intent = new Intent(getApplicationContext(), RankJogos.class);
            intent.putExtra("jogo", "Verminosos");
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_menos_jogo) {
            intent = new Intent(getApplicationContext(), RankNegativosJogos.class);
            intent.putExtra("jogo", "Menos jogou");
            startActivity(intent);
            return true;
        } else if (id == R.id.menos_gol) {
            intent = new Intent(getApplicationContext(), RankNegativoGol.class);
            intent.putExtra("gol", "Menos Gol");
            startActivity(intent);
            return true;
        }  else if (id == R.id.nav_menos_vitorias) {
            intent = new Intent(getApplicationContext(), RankNegativoVitoria.class);
            intent.putExtra("vitoria", "Menos vit�rias");
            startActivity(intent);
            return true;
        }

        finish();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class CarregarListaRaxa extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = MainActivity.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("jogador", String.valueOf(JogadorLogado.idJogador)));
            JSONObject json = jParser.makeHttpRequest(url_consultas_raxas, "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_RAXAS);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String data = c.getString(TAG_DIA);
                        String vitoria = c.getString(TAG_VITORIA);
                        String gol = c.getString(TAG_GOL);

                        HashMap<String, String> map = new HashMap<>();

                        map.put(TAG_ID, id);
                        map.put(TAG_DIA, data);
                        map.put(TAG_VITORIA, vitoria);
                        map.put(TAG_GOL, gol);

                        raxaList.add(map);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * json atualizado para ArrayList
                     * */
                    adapter = new SimpleAdapter(
                            MainActivity.this, raxaList,
                            R.layout.single_post_raxas, new String[] {TAG_ID, TAG_DIA,
                            TAG_VITORIA, TAG_GOL},
                            new int[] {
                                    R.id.single_post_gol_id,
                                    R.id.single_post_data_raxa,
                                    R.id.single_post_vitoria_raxa,
                                    R.id.single_post_gol_raxa});
                    lv.setAdapter(adapter);

                    EditText filter =
                            (EditText) findViewById(R.id.edtTextFiltroRaxa);

                    assert filter != null;

                    filter.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(s.length() > 1) {
                                ((SimpleAdapter) MainActivity.this.adapter).getFilter().filter(s.toString());
                            }

                            if(s.length() == 0) {
                                ((SimpleAdapter) MainActivity.this.adapter).getFilter().filter(s.toString());
                            }

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                }
            });

        }

    }

}
