package telas.login.jogador;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 13/06/2016.
 */
public class EditarRaxa extends AppCompatActivity {

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_raxa_id = "http://webfate.esy.es/raxa/get_raxa_details_id.php";

    private static final String url_raxa_alterar = "http://webfate.esy.es/raxa/update_raxa.php";

    private static final String url_raxa_deletar = "http://webfate.esy.es/raxa/delete_raxa.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RAXAS = "raxas";
    private static final String TAG_ID = "id";
    private static final String TAG_GOL = "gol";
    private static final String TAG_GOLCONTRA = "golContra";
    private static final String TAG_VITORIA = "vitoria";
    private static final String TAG_DERROTA = "derrota";
    private static final String TAG_DIA = "dia";
    private static final String TAG_JOGADOR = "jogador";

    static final int DATE_DIALOG_ID = 0;

    private Button btnAlterar;
    private Button btnExcluir;
    private ImageButton calendario;

    private AlertDialog alerta;

    EditText txtData;
    EditText txtGol;
    EditText txtGolContra;
    EditText txtVitoria;
    EditText txtDerrota;

    static String id;
    static String gol;
    static String golContra;
    static String vitoria;
    static String derrota;
    static String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_raxa);

        calendario = (ImageButton) findViewById(R.id.btnCalendarioE);
        calendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        btnAlterar = (Button) findViewById(R.id.buttonAlterarRaxa);
        btnExcluir = (Button) findViewById(R.id.buttonExcluirRaxa);
        txtData = (EditText) findViewById(R.id.editTextCalendarioE);
        txtGol = (EditText) findViewById(R.id.editTextGolE);
        txtGolContra = (EditText) findViewById(R.id.editTextGolContraE);
        txtVitoria = (EditText) findViewById(R.id.editTextVitoriaE);
        txtDerrota = (EditText) findViewById(R.id.editTextDerrotasE);

        txtData.setEnabled(false);

        Intent i = getIntent();
        id = i.getStringExtra(TAG_ID);

        new GetRaxaById().execute();

        btnAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gol = txtGol.getText().toString();
                golContra = txtGolContra.getText().toString();
                vitoria = txtVitoria.getText().toString();
                derrota = txtDerrota.getText().toString();
                data = txtData.getText().toString();

                if (!data.equals("")) {
                    if (!gol.equals("")) {
                        if (!golContra.equals("")) {
                            if (!vitoria.equals("")) {
                                if (!derrota.equals("")) {
                                    new AlteraRaxa().execute();
                                } else {
                                    menssagem("Campo derrota abrigatório");
                                }
                            } else {
                                menssagem("Campo vitória abrigatório");
                            }
                        } else {
                            menssagem("Campo golContra abrigatório");
                        }
                    } else {
                        menssagem("Campo gol abrigatório");
                    }
                } else {
                    menssagem("Campo data abrigatório");
                }
            }
        });

        btnExcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditarRaxa.this);
                builder.setTitle("Confirmação");
                builder.setMessage("Excluir este arquivo?");
                builder.setPositiveButton("Positivo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        new ExcluirPessoa().execute();
                    }
                });
                builder.setNegativeButton("Negativo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
                alerta = builder.create();
                alerta.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;
        switch (id) {
            case android.R.id.home:
                i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                return (true);
        }

        return true;
    }


    public void menssagem(String menssagem) {
        Toast.makeText(getApplicationContext(), menssagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calendario = Calendar.getInstance();

        int ano = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, ano, mes,
                        dia);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String d = String.valueOf(dayOfMonth);
            String m =  String.valueOf(monthOfYear+1);
            String a =  String.valueOf(year);

            String dia = null;
            String mes = null;

            if (d.length() < 2) {
                dia = "0"+d;
            } else {
                dia = d;
            }

            if(m.length() < 2) {
                mes = "0"+m;
            } else {
                mes = m;
            }


            txtData.setText(dia + "/" + mes + "/" + a);
            data = dia + "/" + mes + "/" + a;
        }
    };

    class GetRaxaById extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarRaxa.this);
            pDialog.setMessage("Processando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("id", id));

                        StrictMode.ThreadPolicy policy =
                                new StrictMode.ThreadPolicy.Builder().
                                        permitAll().build();

                        StrictMode.setThreadPolicy(policy);

                        JSONObject json = jsonParser.makeHttpRequest(
                                url_raxa_id, "POST", params);

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {
                            JSONArray productObj = json
                                    .getJSONArray("raxas");
                            JSONObject pendaftaran = productObj
                                    .getJSONObject(0);

                            txtData.setText(pendaftaran.getString(TAG_DIA));
                            txtDerrota.setText(pendaftaran.getString(TAG_DERROTA));
                            txtVitoria.setText(pendaftaran.getString(TAG_VITORIA));
                            txtGol.setText(pendaftaran.getString(TAG_GOL));
                            txtGolContra.setText(pendaftaran.getString(TAG_GOLCONTRA));

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class AlteraRaxa extends AsyncTask<String, String, String> {

        private ProgressDialog alterar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alterar = new ProgressDialog(EditarRaxa.this);
            alterar.setMessage("Carregando dados ...");
            alterar.setIndeterminate(false);
            alterar.setCancelable(true);
            alterar.show();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair(TAG_ID, id));
            params.add(new BasicNameValuePair(TAG_GOL, gol));
            params.add(new BasicNameValuePair(TAG_GOLCONTRA, golContra));
            params.add(new BasicNameValuePair(TAG_VITORIA, vitoria));
            params.add(new BasicNameValuePair(TAG_DERROTA, derrota));
            params.add(new BasicNameValuePair(TAG_DIA, data));

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            JSONObject json = jsonParser.makeHttpRequest(
                    url_raxa_alterar, "POST", params);
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            alterar.dismiss();
        }
    }

    class ExcluirPessoa extends AsyncTask<String, String, String> {

        ProgressDialog excluir;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            excluir = new ProgressDialog(EditarRaxa.this);
            excluir.setMessage("Excluindo...");
            excluir.setIndeterminate(false);
            excluir.setCancelable(true);
            excluir.show();
        }

        protected String doInBackground(String... args) {
            int success;
            try {
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("id", id));

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                JSONObject json = jsonParser.makeHttpRequest(
                        url_raxa_deletar, "POST", params);
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            excluir.dismiss();
        }

    }

}
