package telas.login.jogador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import json.JSONParser;

/**
 * Created by jsj on 30/12/2016.
 */
public class Comparacao extends AppCompatActivity {

    private TextView gol1, gol2, vitoria1, vitoria2, golContra1, golContra2, derrota1, derrota2,
    golSaldo, vitoriaSaldo, golContraSaldo, derrotaSaldo, jogador1, jogador2, saldoInfo;

    private static String url_consultas_comparacao = "http://webfate.esy.es/raxa/comparacao.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RAXAS = "raxas";
    private static final String TAG_GOL = "gol";
    private static final String TAG_ID = "id";
    private static final String TAG_GOLCONTRA = "golContra";
    private static final String TAG_VITORIA = "vitoria";
    private static final String TAG_DERROTA = "derrota";

    JSONArray empresaJson = null;
    JSONParser jParser = new JSONParser();

    String golContraData = "";
    String golData = "";
    String derrotaData = "";
    String vitoriaData = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.comparacao);
        saldoInfo = (TextView) findViewById(R.id.infoSaldo);

        gol1 = (TextView) findViewById(R.id.gol1);
        gol2 = (TextView) findViewById(R.id.gol2);

        vitoria1 = (TextView) findViewById(R.id.vitoria1);
        vitoria2 = (TextView) findViewById(R.id.vitoria2);

        golContra1 = (TextView) findViewById(R.id.golcontra1);
        golContra2 = (TextView) findViewById(R.id.golcontra2);

        derrota1 = (TextView) findViewById(R.id.derrota1);
        derrota2 = (TextView) findViewById(R.id.derrota2);

        golSaldo = (TextView) findViewById(R.id.golsaldo);
        vitoriaSaldo = (TextView) findViewById(R.id.vitoriasaldo);
        golContraSaldo = (TextView) findViewById(R.id.golcontrasaldo);
        derrotaSaldo = (TextView) findViewById(R.id.derrotasaldo);
        jogador1 = (TextView) findViewById(R.id.jogador1);
        jogador2 = (TextView) findViewById(R.id.jogador2);

        jogador1.setText("Voc�");
        jogador2.setText(JogadorLogado.nomeJodagorLinhaTempo);
        saldoInfo.setText("Saldo em rela��o ao " + JogadorLogado.nomeJodagorLinhaTempo);
        new CarregarComparacao().execute();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;
        switch (id) {
            case android.R.id.home:
                finish();
            break;
        }
        return true;
    }

    class CarregarComparacao extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = Comparacao.this;
        List<Map<String, String>> data = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Calculando resultado...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {
            return null;
        }


        protected void onPostExecute(String file_url) {
            this.pDialog.dismiss();
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("jogador", JogadorLogado.idJogador.toString()+","+JogadorLogado.idJodagorLinhaTempo));
            Log.i("ids", JogadorLogado.idJogador.toString() + "," + JogadorLogado.idJodagorLinhaTempo);
            JSONObject json = jParser.makeHttpRequest(url_consultas_comparacao, "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                Log.i("success", String.valueOf(success));

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_RAXAS);
                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);
                        if (i == 0){
                            gol1.setText(transformy(c.getString(TAG_GOL)));
                            golContra1.setText(transformy(c.getString(TAG_GOLCONTRA)));
                            vitoria1.setText(transformy(c.getString(TAG_VITORIA)));
                            derrota1.setText(transformy(c.getString(TAG_DERROTA)));
                        } else {
                            gol2.setText(transformy(c.getString(TAG_GOL)));
                            golContra2.setText(transformy(c.getString(TAG_GOLCONTRA)));
                            vitoria2.setText(transformy(c.getString(TAG_VITORIA)));
                            derrota2.setText(transformy(c.getString(TAG_DERROTA)));
                        }
                    }

                    Integer golresult = Integer.parseInt(gol1.getText().toString()) - Integer.parseInt(gol2.getText().toString());
                    Integer golContraResult = Integer.parseInt(golContra1.getText().toString()) - Integer.parseInt(golContra2.getText().toString());
                    Integer derrotaResult = Integer.parseInt(derrota1.getText().toString()) - Integer.parseInt(derrota2.getText().toString());
                    Integer vitoriaResult = Integer.parseInt(vitoria1.getText().toString()) - Integer.parseInt(vitoria2.getText().toString());

                    golSaldo.setText(golresult.toString());
                    golContraSaldo.setText(golContraResult.toString());
                    derrotaSaldo.setText(derrotaResult.toString());
                    vitoriaSaldo.setText(vitoriaResult.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public String transformy (String string){
        if(string.equals("null")){
            string = "0";
        }
        return string;
    }

}
