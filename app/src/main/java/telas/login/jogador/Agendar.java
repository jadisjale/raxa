package telas.login.jogador;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.jsj.raxa.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

public class Agendar extends BroadcastReceiver {

    private static String url_consultas_notificacao_ativa = "http://webfate.esy.es/raxa/get_notificacao_ativa.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TITULO = "titulo";
    private static final String ID = "id";
    private static final String TEXTO = "texto";
    private static final String DATA = "data";
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> listEnquete;
    private int success;
    JSONArray empresaJson = null;
    private String titulo;
    private String id;
    private String texto;

    public Agendar(String titulo, String texto) {
        this.titulo = titulo;
        this.texto = texto;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        new CarregarNotificacaoAtiva().execute();

        if (1 != 1){
            intent = new Intent();
            PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
            Notification noti = new Notification.Builder(context)
                    .setTicker("Ticker Title")
                    .setContentTitle(this.titulo)
                    .setContentText(this.texto)
                    .setSmallIcon(R.drawable.bola)
                    .setContentIntent(pIntent).getNotification();
            noti.defaults |= Notification.DEFAULT_SOUND;
            long[] vibrate = { 0, 100, 200, 300 };
            noti.vibrate = vibrate;

            noti.flags=Notification.FLAG_AUTO_CANCEL;
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, noti);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.bola)
                    .setContentTitle("My notification")
                    .setContentText("Hello World!");
        }
    }

    class CarregarNotificacaoAtiva extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            JSONObject json = jParser.makeHttpRequest(url_consultas_notificacao_ativa, "GET", params);

            try {
                success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(DATA);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);

                        id = c.getString(ID);
                        titulo = c.getString(TITULO);
                        texto = c.getString(TEXTO);

                        Log.i("titulo", titulo);

                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {

        }

    }
}
