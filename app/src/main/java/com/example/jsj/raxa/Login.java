package com.example.jsj.raxa;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.JSONParser;
import telas.login.jogador.JogadorLogado;
import telas.login.jogador.NovoLogin;
import telas.login.jogador.SalvarJogador;

public class Login extends AppCompatActivity {

     String nome;

    public static PendingIntent pendingIntent;
    public static AlarmManager alarme;

     EditText inputNome;
     EditText inputSenha;

     Button logar;
     Button cadastrarJogador;

     ProgressDialog pDialog;

     static final String TAG_SUCCESS = "success";

     static final String url_pessoa_login = "http://webfate.esy.es/raxa/get_dtl_jogador_by_nome.php";

    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        inputNome = (EditText) findViewById(R.id.jogadorNameS);
        //chamando a tela para cadastrar um jogador
        cadastrarJogador = (Button) findViewById(R.id.btnJogador);
        cadastrarJogador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SalvarJogador.class);
                startActivity(intent);
            }
        });
        logar = (Button) findViewById(R.id.btnLogin);
        logar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nome = inputNome.getText().toString();
                if (nome.length() > 2 && !nome.equals("")) {
                    new VerificaLogin().execute();
                } else {
                    menssagem("Campo nome insuficiente");
                }
            }
        });

//        Intent intent = new Intent(this, Agendar.class); //chama a classe agendar
//        programarAlarme(this, intent, getTime());
//        repetirAlarme(this, intent, getTime());
//
//        menssagem("Receber� novo");
    }

    public long getTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.add(Calendar.SECOND, 5);
        long time = c.getTimeInMillis();
        return time;
    }

    public static void repetirAlarme(Context context, Intent intent, long time) {
        pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarme = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarme.setRepeating(AlarmManager.ELAPSED_REALTIME, 0, 10000, pendingIntent); //10 em 10 segundos
    }

    public static void programarAlarme(Context context, Intent intent, long time) {
        pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarme = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarme.set(AlarmManager.RTC_WAKEUP, time, pendingIntent); //uma vez em 5 segundos
    }

    public void menssagem(String menssagem){
        Toast.makeText(getApplicationContext(), menssagem, Toast.LENGTH_LONG).show();
    }

    //verifica se existe o login
    class VerificaLogin extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Autenticando...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {
                        List<NameValuePair> params = new ArrayList<>();
                        params.add(new BasicNameValuePair("nome", nome));

                        StrictMode.ThreadPolicy policy =
                                new StrictMode.ThreadPolicy.Builder().
                                        permitAll().build();

                        StrictMode.setThreadPolicy(policy);

                        JSONObject json = jsonParser.makeHttpRequest(
                                url_pessoa_login, "POST", params);

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {
                            JSONArray productObj = json
                                    .getJSONArray("pessoas");
                            JSONObject pendaftaran = productObj
                                    .getJSONObject(0);
                            JogadorLogado.idJogador = Integer.parseInt(pendaftaran.getString("id"));
                            JogadorLogado.nomeJogador = pendaftaran.getString("nome");
                            JogadorLogado.senhaJogador =  pendaftaran.getString("senha");
                            JogadorLogado.file = pendaftaran.getString("file");

                            Intent intent = new Intent(Login.this, NovoLogin.class);

                            startActivity(intent);

                        } else {
                            inputNome.setText("");
                            Toast.makeText(getApplicationContext(), "Jogador n�o encontrado", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }


}
